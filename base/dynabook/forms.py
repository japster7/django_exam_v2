from django.contrib.auth import models as am, forms as af
from django.utils.text import slugify
from django import forms as f

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

from dynabook import models as dm


class RegistrationForm(af.UserCreationForm):
    last_name = f.CharField(max_length=100)
    first_name = f.CharField(max_length=100)
    email = f.EmailField()

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.wrapper_class = 'row m-1'
        self.helper.field_class = 'col-md-9'
        self.helper.label_class = 'col-md-3'
        self.helper.form_class = 'p-0 pb-1 bg-light'
        self.helper.add_input(
            Submit(
                'submit', 'Submit',
                css_class='btn btn-primary'
            )
        )

    def save(self, commit=True):
        user = super(RegistrationForm, self).save()
        dm.Person.objects.get_or_create(user=user, slug=slugify(user.username))
        return user

    class Meta:
        model = am.User
        fields = ('username', 'last_name', 'first_name', 'email')


class FriendFormSetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(FriendFormSetHelper, self).__init__(*args, **kwargs)
        self.add_input(Submit('submit', 'Submit'))
        self.template = 'dynabook/table_inline_formset.html'


class FriendFormSet(f.BaseInlineFormSet):
    def clean(self):
        if any(self.errors):
            return
        persons = []
        for form in self.forms:
            data = form.cleaned_data.copy()
            if data:
                field_name = 'from_person' if \
                    form.__dict__['fields']['to_person'].widget.__class__.__name__ \
                    == 'HiddenInput' else 'to_person'
                person = data[field_name]
                if person in persons:
                    form.add_error(field_name, "Persons must be distinct")
                if data['from_person'] == data['to_person']:
                    form.add_error(field_name, "Cannot be the same person")
                persons.append(person)
