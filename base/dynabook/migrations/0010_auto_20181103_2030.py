# Generated by Django 2.1.2 on 2018-11-03 12:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dynabook', '0009_auto_20181102_2130'),
    ]

    operations = [
        migrations.AlterField(
            model_name='person',
            name='friends',
            field=models.ManyToManyField(related_name='user_friends', through='dynabook.Friend', to='dynabook.Person'),
        ),
    ]
