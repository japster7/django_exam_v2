from django.urls import path
from django.views import generic as gv
from django.contrib.auth import views as av

from dynabook import views as dv, forms as df

app_name = "dynabook"
urlpatterns = [
    path('', gv.TemplateView.as_view(template_name='base.html'), name='index'),
    path(
        'login/',
        av.LoginView.as_view(
            template_name='dynabook/login.html',
            extra_context={'no_nav': True},
            redirect_authenticated_user=True,
        ),
        name='login'
    ),
    path('logout/', av.LogoutView.as_view(), name='logout'),
    path(
        'register/',
        dv.RegisterView.as_view(
            extra_context={
                'form': df.RegistrationForm(),
                'no_nav': True,
                'title': 'Register',
            },
        ),
        name='register'
    ),
    path('popular/', dv.PopularUsersView.as_view(), name='popular_users'),
    path('search/', dv.SearchUserView.as_view(), name='search_user'),
    path('export/', dv.export_to_csv, name='export'),
    path(
        'password/change/',
        dv.PasswordUpdateView.as_view(),
        name='change_password'
    ),
    path('<slug:slug>/', dv.ProfileView.as_view(), name='profile'),
    path(
        '<slug:slug>/edit/',
        dv.ProfileEditView.as_view(),
        name='profile_edit'
    ),
    path(
        '<slug:slug>/edit/staff/',
        dv.ProfileEditStaffView.as_view(),
        name='profile_edit_staff'
    ),
    path(
        '<slug:slug>/manage/<str:direction>/',
        dv.ManageFriendsStaffView.as_view(),
        name='manage_friends'
    ),
    path(
        '<slug:slug>/change/<int:status>/',
        dv.change_status,
        name='change_status'
    ),
]
