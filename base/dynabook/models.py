from django.db import models
from django.db.models import Q
from django.contrib.auth.models import User
from django.contrib.messages import constants as messages

# Create your models here.
status = {
    'N': {'value': 0, 'text': 'None',
          'tag': messages.INFO, 'message': 'None!'},
    'P': {'value': 1, 'text': 'Pending',
          'tag': messages.INFO, 'message': 'Friend request added!'},
    'A': {'value': 2, 'text': 'Accepted',
          'tag': messages.INFO, 'message': 'Friend request accepted!'},
    'B': {'value': 3, 'text': 'Blocked',
          'tag': messages.ERROR, 'message': 'Person blocked!'},
    'C': {'value': 4, 'text': 'Cancelled',
          'tag': messages.WARNING, 'message': 'Friend request cancelled!'},
    'R': {'value': 5, 'text': 'Rejected',
          'tag': messages.WARNING, 'message': 'Friend request rejected!'},
    'U': {'value': 6, 'text': 'Unblocked',
          'tag': messages.WARNING, 'message': 'Person unblocked!'},
    'D': {'value': 7, 'text': 'Defriended',
          'tag': messages.WARNING, 'message': 'Person defriended!'},
}
status_choices = (
    (status['N']['value'], status['N']['text']),
    (status['P']['value'], status['P']['text']),
    (status['A']['value'], status['A']['text']),
    (status['B']['value'], status['B']['text']),
    (status['C']['value'], status['C']['text']),
    (status['R']['value'], status['R']['text']),
    (status['U']['value'], status['U']['text']),
    (status['D']['value'], status['D']['text']),
)


def get_status_key(value):
    for key, values in status.items():
        if values['value'] == value:
            return key
    return None


class Person(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    friends = models.ManyToManyField(
        'self',
        through='Friend',
        symmetrical=False,)
    slug = models.SlugField(unique=True)

    def __str__(self):
        return self.user.get_full_name()

    # connection request to other persons
    def get_connect_from_self(self, person):
        try:
            return Friend.objects.get(
                from_person=self,
                to_person=person,
                active=True)
        except Friend.DoesNotExist:
            return None

    # connection request from other persons
    def get_connect_to_self(self, person):
        try:
            return Friend.objects.get(
                from_person=person,
                to_person=self,
                active=True)
        except Friend.DoesNotExist:
            return None

    def get_connection(self, person):
        friend = self.get_connect_from_self(person)
        if friend:
            return friend
        friend = self.get_connect_to_self(person)
        if friend:
            return friend
        return None

    def get_connect_type(self, person):
        if self.get_connect_from_self(person):
            return 'source'
        elif self.get_connect_to_self(person):
            return 'target'
        return None

    def connect(self, person):
        if self == person:
            return None
        friend = self.get_connection(person)
        if not friend:
            friend = Friend.objects.create(from_person=self, to_person=person)
        return friend

    def change_status(self, person, status_value):
        if status_value == status['B']['value']:
            return self.block_person(person)
        else:
            friend = self.connect(person)
            if friend:
                if status_value > status['B']['value']:
                    friend.active = False
                friend.status = status_value
                friend.save()
                return friend
        return None

    def get_status(self, person):
        friend = self.get_connection(person)
        if not friend:
            return status['N']['value']
        return friend.status

    def block_person(self, person):
        try:
            friend = Friend.objects.filter(
                (
                    (Q(from_person=self) & Q(to_person=person)) |
                    (Q(from_person=person) & Q(to_person=self))
                ) &
                Q(active=True) &
                ~Q(status=status['B']['value'])
            )
            friend.update(active=False)

        except Friend.DoesNotExist:
            pass
        return Friend.objects.create(
            from_person=self,
            to_person=person,
            status=status['B']['value'])

    def get_friends(self):
        friends_from = Friend.objects.filter(
            status=status['A']['value'],
            active=True,
            from_person=self)
        friends_to = Friend.objects.filter(
            status=status['A']['value'],
            active=True,
            to_person=self)
        friends_from_list = [
            {
                'name': friend.to_person.user.get_full_name(),
                'slug': friend.to_person.slug
            }
            for friend in friends_from
        ]
        friends_to_list = [
            {
                'name': friend.from_person.user.get_full_name(),
                'slug': friend.from_person.slug
            }
            for friend in friends_to
        ]
        return friends_from_list + friends_to_list

    def get_friends_query(self):
        return Friend.objects.filter(
            Q(status=status['A']['value'], active=True) &
            (Q(from_person=self) | Q(to_person=self)))

    def get_friends_count(self):
        return self.get_friends_query().count()

    def get_friends_by_date_count(self, time):
        return self.get_friends_query().filter(date_updated__gte=time).count()

    def get_blocked(self):
        connections = Friend.objects.filter(
            to_person=self,
            status=status['B']['value'],
            active=True
        )
        return [conn.from_person.pk for conn in connections]

    def get_defriended_count(self):
        return Friend.objects.filter(
            Q(status=status['D']['value']) &
            (Q(from_person=self) | Q(to_person=self))).count()


class Friend(models.Model):
    from_person = models.ForeignKey(
        Person,
        on_delete=models.CASCADE,
        related_name='source',
        null=True
    )
    to_person = models.ForeignKey(
        Person,
        on_delete=models.CASCADE,
        related_name='target',
        null=True
    )
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)
    status = models.IntegerField(choices=status_choices, default=0)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.from_person.slug + " - " + self.to_person.slug
