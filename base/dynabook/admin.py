from django.contrib import admin
from dynabook import models as dm

# Register your models here.
admin.site.register(dm.Person)
admin.site.register(dm.Friend)
