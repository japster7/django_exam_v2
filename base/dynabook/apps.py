from django.apps import AppConfig


class DynabookConfig(AppConfig):
    name = 'dynabook'
