import csv
import datetime

from django.http import Http404, HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy, reverse
from django.views.generic import (
    TemplateView, CreateView, DetailView, ListView, UpdateView
)
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.views import PasswordChangeView
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.contrib.auth import models as am
from django.db.models import Q
from django.utils.text import slugify
from django.utils import timezone
from django.forms import inlineformset_factory

from dynabook import forms as df, models as dm


def filter_user(request, person=None):
    result = dm.Person.objects.all()
    if person and not request.user.is_staff:
        blocked = person.get_blocked()
        result = result.exclude(pk__in=blocked)
    name_query = request.GET.get('q')
    if name_query:
        result = result.filter(
            Q(user__last_name__icontains=name_query) |
            Q(user__first_name__icontains=name_query)
        )
    date_query = request.GET.get('o')
    if date_query:
        date_query = date_query.lower()
        date_query_selection = ['yesterday', 'lastweek', 'lastmonth']
        if date_query in date_query_selection:
            if date_query == 'lastmonth':
                value = datetime.timedelta(days=30)
            elif date_query == 'lastweek':
                value = datetime.timedelta(days=7)
            else:
                value = datetime.timedelta(days=1)
            time = timezone.now() - value
            result = result.filter(user__date_joined__gte=time)
    result.order_by(
        'user__last_name',
        'user__first_name',
        '-user__date_joined'
    )
    sort = request.GET.get('s')
    if sort and sort in [
        'name', '-name',
        'date_joined', '-date_joined',
        'count', '-count',
        'dcount', '-dcount',
    ]:
        if sort == 'name':
            result = sorted(
                result,
                key=lambda r: (r.user.last_name, r.user.first_name)
            )
        elif sort == '-name':
            result = sorted(
                result,
                key=lambda r: (r.user.last_name, r.user.first_name),
                reverse=True
            )
        elif sort == 'date_joined':
            result = sorted(
                result,
                key=lambda r: r.user.date_joined
            )
        elif sort == '-date_joined':
            result = sorted(
                result,
                key=lambda r: r.user.date_joined,
                reverse=True
            )
        elif sort == 'count':
            result = sorted(
                result,
                key=lambda r: r.get_friends_count()
            )
        elif sort == '-count':
            result = sorted(
                result,
                key=lambda r: r.get_friends_count(),
                reverse=True
            )
        elif sort == 'dcount':
            result = sorted(
                result,
                key=lambda r: r.get_defriended_count(),
            )
        elif sort == '-dcount':
            result = sorted(
                result,
                key=lambda r: r.get_defriended_count(),
                reverse=True
            )
    return result


# Create your views here.
class SearchUserView(ListView):
    paginate_by = 10
    context_object_name = 'search_results'
    template_name = 'dynabook/search_user.html'

    def get_queryset(self):
        try:
            return filter_user(self.request, self.request.user.person)
        except AttributeError:
            return filter_user(self.request)


class RegisterView(CreateView):
    form_class = df.RegistrationForm
    success_url = reverse_lazy('dynabook:login')
    template_name = 'dynabook/register.html'


class ProfileView(DetailView):
    model = dm.Person
    template_name = 'dynabook/profile_view.html'

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        person = dm.Person.objects.get(slug=self.kwargs['slug'])
        context['status'] = dm.status
        context['friends'] = person.get_friends()
        if self.request.user.is_authenticated:
            try:
                user = self.request.user.person
            except dm.Person.DoesNotExist:
                user = None
            # Determine buttons to show
            if user:
                if user == person:
                    context['status_self'] = True
                else:
                    relation_status = user.get_status(person)
                    relation_type = user.get_connect_type(person)
                    if relation_status == dm.status['P']['value']:
                        if relation_type == 'source':
                            context['status_cancel_request'] = True
                        else:
                            context['status_accept_or_reject_request'] = True
                    elif relation_status == dm.status['A']['value']:
                        context['status_friend'] = True
                    elif relation_status == dm.status['B']['value']:
                        if relation_type == 'source':
                            context['status_block_source'] = True
                        else:
                            context['status_block_target'] = True
        return context


class ProfileEditView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = am.User
    template_name = 'dynabook/profile_edit.html'
    fields = ('username', 'last_name', 'first_name', 'email')
    success_message = 'Profile Edited!'

    def get_object(self, queryset=None):
        return am.User.objects.get(person__slug=self.kwargs['slug'])

    def get_success_url(self):
        return reverse_lazy('dynabook:profile', args=(self.kwargs['slug'],))

    def form_valid(self, form):
        person = dm.Person.objects.get(slug=self.kwargs['slug'])
        slug = slugify(form.cleaned_data['username'])
        person.slug = slug
        self.kwargs['slug'] = slug
        person.save()
        return super().form_valid(form)


class StaffTestMixin(UserPassesTestMixin):
    def test_func(self):
        return staff_test(self.request.user)


class ProfileEditStaffView(StaffTestMixin, ProfileEditView):
    fields = (
        'username', 'last_name', 'first_name', 'email',
        'is_active', 'is_staff', 'is_superuser',
    )
    login_url = reverse_lazy('dynabook:login')


class PasswordUpdateView(SuccessMessageMixin, PasswordChangeView):
    template_name = 'dynabook/password_change.html'
    success_url = reverse_lazy('dynabook:home')
    success_message = 'Password Changed!'


@login_required
def change_status(request, slug, status):
    person = dm.Person.objects.get(slug=slug)
    try:
        user = dm.Person.objects.get(user=request.user)
    except dm.Person.DoesNotExist:
        user = None
    if user and request.user.person.change_status(person, status):
        key = dm.get_status_key(status)
        messages.add_message(
            request,
            dm.status[key]['tag'],
            dm.status[key]['message']
        )
    else:
        messages.add_message(request, messages.ERROR, 'Action not allowed!')
    return redirect(reverse('dynabook:profile', args=(slug,)))


def staff_test(user):
    if user.is_staff:
        return True
    raise Http404()


@login_required
@user_passes_test(staff_test)
def export_to_csv(request):
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="user_data.csv"'
    # Create the csv file
    writer = csv.writer(response)
    head = ['Name', 'Date Joined', 'Friend Count']
    if request.user.is_staff:
        head.append('Defriend Count')
    writer.writerow(head)
    # Get rows to write
    result = filter_user(request)
    # Write rows to file
    for person in result:
        row = [
            person.user.get_full_name(),
            person.user.date_joined,
            person.get_friends_count()
        ]
        if request.user.is_staff:
            row.append(person.get_defriended_count())
        writer.writerow(row)
    return response


def create_friends_formsets(person, direction='from', post=None):
    if direction == 'to':
        query = dm.Friend.objects.filter(from_person=person, active=1)
        fk_field, form_field = 'from_person', 'to_person'
    else:
        query = dm.Friend.objects.filter(to_person=person, active=1)
        fk_field, form_field = 'to_person', 'from_person'
    PersonInlineFormSet = inlineformset_factory(
        dm.Person, dm.Friend,
        fk_name=fk_field,
        fields=(form_field, 'status'),
        formset=df.FriendFormSet,
        extra=5)
    PersonFormset = PersonInlineFormSet(post, instance=person, queryset=query)
    return PersonFormset


class ManageFriendsStaffView(LoginRequiredMixin, StaffTestMixin, TemplateView):
    def get(self, request, *args, **kwargs):
        direction = kwargs['direction']
        if direction.lower() not in ('to', 'from'):
            raise Http404()
        slug = kwargs['slug']
        person = dm.Person.objects.get(slug=slug)
        helper = df.FriendFormSetHelper()
        personFormSet = create_friends_formsets(person, direction)
        return render(
            request,
            'dynabook/manage_friends_staff.html',
            {
                'formset': personFormSet,
                'helper': helper,
                'direction': direction,
            }
        )

    def post(self, request, *args, **kwargs):
        slug = kwargs['slug']
        person = dm.Person.objects.get(slug=slug)
        helper = df.FriendFormSetHelper()
        direction = kwargs['direction']
        personFormSet = create_friends_formsets(
            person, direction, request.POST
        )
        if personFormSet.is_valid():
            friends = personFormSet.save(commit=False)
            for friend in friends:
                person = dm.Person.objects.get(pk=friend.from_person.pk)
                person.change_status(friend.to_person, friend.status)
            return redirect(reverse('dynabook:profile', args=(slug,)))
        return render(
            request,
            'dynabook/manage_friends_staff.html',
            {
                'formset': personFormSet,
                'helper': helper,
                'direction': direction,
            }
        )


class PopularUsersView(ListView):
    context_object_name = 'popular_users_results'
    template_name = 'dynabook/popular_users.html'

    def get_queryset(self):
        data = dm.Person.objects.all()
        period_query = self.request.GET.get('f')
        if period_query and period_query in [
            'daily', 'weekly', 'monthly', 'yearly'
        ]:
            if period_query == 'yearly':
                time_value = datetime.timedelta(days=365)
            elif period_query == 'monthly':
                time_value = datetime.timedelta(days=30)
            elif period_query == 'weekly':
                time_value = datetime.timedelta(days=7)
            else:
                time_value = datetime.timedelta(days=1)
            time = timezone.now() - time_value
            data = sorted(
                data,
                key=lambda d: d.get_friends_by_date_count(time),
                reverse=True
            )
        else:
            data = sorted(
                data,
                key=lambda d: d.get_friends_count(),
                reverse=True
            )
        return data[:10]
